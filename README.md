This scraper downloads the iFixit resources (categories, guides, ...) and puts them in a ZIM file, a clean and user friendly format for storing content for offline usage.

For now, this tool is still under active development. Most recent version of the tool is located in the `develop` branch or any of its sub-branches.